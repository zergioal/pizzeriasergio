﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using CapaDatos;
using CapaEntidades;

namespace CapaNegocios
{
    public class N_Tamanos
    {
        readonly D_Tamanos ObjTamanos = new D_Tamanos();

        public static DataTable MostrarRegistros()
        {
            return new D_Tamanos().MostrarRegistros();
        }

        public void InsertarRegistros(E_Tamanos tamanos)
        {
            ObjTamanos.InsertarRegistros(tamanos);
        }

        public void EditarRegistros (E_Tamanos tamanos)
        {
            ObjTamanos.EditarRegistros(tamanos);
        }

        public void EliminarRegistros (E_Tamanos tamanos)
        {
            ObjTamanos.EliminarRegistros(tamanos);
        }
    }
}
