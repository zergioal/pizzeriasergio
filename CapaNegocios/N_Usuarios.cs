﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using CapaDatos;
using CapaEntidades;

namespace CapaNegocios
{
    public class N_Usuarios
    {
        readonly D_Usuarios ObjUsuarios = new D_Usuarios();

        public List<E_Usuarios> ListarUsuarios()
        {
            return ObjUsuarios.ListarRegistros();
        }

        public void InsertarRegistros(E_Usuarios usuarios)
        {
            ObjUsuarios.InsertarRegistros(usuarios);
        }

        public void EditarRegistros(E_Usuarios usuarios)
        {
            ObjUsuarios.EditarRegistros(usuarios);
        }

        public void EliminarRegistros(E_Usuarios usuarios)
        {
            ObjUsuarios.EliminarRegistros(usuarios);
        }

    }
}
