﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using CapaDatos;
using CapaEntidades;

namespace CapaNegocios
{
    public class N_Pizzas
    {
        readonly D_Pizzas ObjPizzas = new D_Pizzas();

        public static DataTable MostrarRegistros()
        {
            return new D_Pizzas().MostrarRegistros();
        }
        public static DataTable BuscarRegistros(string textobuscar)
        {
            return new D_Pizzas().BuscarRegistros(textobuscar);
        }

        public void InsertarRegistros(E_Pizzas pizzas)
        {
            ObjPizzas.InsertarRegistros(pizzas);
        }

        public void EditarRegistros(E_Pizzas pizzas)
        {
            ObjPizzas.EditarRegistros(pizzas);
        }

        public void EliminarRegistros(E_Pizzas pizzas)
        {
            ObjPizzas.EliminarRegistros(pizzas);
        }
    }
}
