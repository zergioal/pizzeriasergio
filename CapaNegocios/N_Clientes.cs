﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using CapaDatos;
using CapaEntidades;

namespace CapaNegocios
{
    public class N_Clientes
    {
        readonly D_Clientes ObjClientes = new D_Clientes();

        public static DataTable MostrarRegistros()
        {
            return new D_Clientes().MostrarRegistros();
        }
        public static DataTable BuscarRegistros(string textobuscar)
        {
            return new D_Clientes().BuscarRegistros(textobuscar);
        }

        public void InsertarRegistros(E_Clientes clientes)
        {
            ObjClientes.InsertarRegistros(clientes);
        }

        public void EditarRegistros(E_Clientes clientes)
        {
            ObjClientes.EditarRegistros(clientes);
        }

        public void EliminarRegistros(E_Clientes clientes)
        {
            ObjClientes.EliminarRegistros(clientes);
        }
    }
}
