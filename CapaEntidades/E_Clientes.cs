﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Clientes
    {
        public int IdClientes { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string CioNit { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

    }
}
