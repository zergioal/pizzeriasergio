﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Tamanos
    {
        public int IdTamano { get; set; }
        public string Descripcion { get; set; }
    }
}
