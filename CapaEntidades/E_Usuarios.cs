﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Usuarios
    {
        public int IdUsuarios { get; set; }
        public string Usuarios { get; set; }
        public string Contraseña { get; set; }
        public string Acceso { get; set; }
    }
}
