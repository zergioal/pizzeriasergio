﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Pizzas
    {
        public int IdPizzas { get; set; }
        public string Nombre { get; set; }
        public string Caracteristicas { get; set; }
        public decimal PrecioVenta { get; set; }
        public decimal CostoProduccion { get; set; }
        public int IdTamano { get; set; }
    }
}
