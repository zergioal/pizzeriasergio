﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;

namespace CapaPresentacion
{
    public partial class FrmMenu : Form
    {
        //public string Acceso;
        public FrmMenu(string Acceso)
        {
            InitializeComponent();
            CargarDatosUsuarios();
            GestionAcceso(Acceso);
        }

        private void CargarDatosUsuarios()
        {
            LblAcceso.Text = E_InicioSesion.Acceso;
            LblUsuario.Text = E_InicioSesion.Usuario;
        }

        private void GestionAcceso(String Acceso)
        {
            if (Acceso == "Administrador")
            {
                MessageBox.Show("Bienvenido Admin", "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (Acceso == "Cajero")
            {
                MessageBox.Show("Ingreso permitido: CAJERO", "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);

                BtnUsuarios.Enabled = false;
                BtnTamanos.Enabled = false;
                BtnPizzas.Enabled = false;
            }
            else
            {
                MessageBox.Show("Ingreso sospechoso", "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }


        private const int tamañogrid = 10;
        private const int areamouse = 132;
        private const int botonizquierdo = 17;
        private Rectangle rectangulogrid;

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            var region = new Region(new Rectangle(0, 0, ClientRectangle.Width, ClientRectangle.Height));

            rectangulogrid = new Rectangle(ClientRectangle.Width - tamañogrid, ClientRectangle.Height - tamañogrid, tamañogrid, tamañogrid);

            region.Exclude(rectangulogrid);

            PanelPrincipal.Region = region;
            Invalidate();
        }

        protected override void WndProc(ref Message sms)
        {
            switch(sms.Msg)
            {
                case areamouse:
                    base.WndProc(ref sms);

                    var RefPoint = PointToClient(new Point(sms.LParam.ToInt32() & 0xffff, sms.LParam.ToInt32() >> 16));

                    if(!rectangulogrid.Contains(RefPoint))
                    {
                        break;
                    }

                    sms.Result = new IntPtr(botonizquierdo);
                    break;
                
                default:
                    base.WndProc(ref sms);
                    break;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            SolidBrush solidBrush = new SolidBrush(Color.FromArgb(55, 61, 69));

            e.Graphics.FillRectangle(solidBrush, rectangulogrid);

            base.OnPaint(e);

            ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, rectangulogrid);
        }

        int lx, ly;
        int sw, sh;

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de cerrar el programa?","Alerta", MessageBoxButtons.YesNo)==DialogResult.Yes)     
            {
                Application.Exit();
            }
        }

        private void BtnRestaurar_Click(object sender, EventArgs e)
        {
            Size = new Size(sw, sh);
            Location = new Point(lx, ly);

            BtnRestaurar.Visible = false;
            BtnMaximizar.Visible = true;
        }

        private void BtnMaximizar_Click(object sender, EventArgs e)
        {
            lx = Location.X;
            ly = Location.Y;
            sw = Size.Width;
            sh = Size.Height;

            Size = Screen.PrimaryScreen.WorkingArea.Size;
            Location = Screen.PrimaryScreen.WorkingArea.Location;

            BtnMaximizar.Visible = false;
            BtnRestaurar.Visible = true;
        }

        private void BtnClientes_Click(object sender, EventArgs e)
        {
            AbrirFormularios<FrmListadoClientes>();
        }

        private void BtnPizzas_Click(object sender, EventArgs e)
        {
            AbrirFormularios<FrmListadoPizzas>();
        }

        private void BtnTamanos_Click(object sender, EventArgs e)
        {
            AbrirFormularios<FrmTamanos>();
        }

        private void BtnUsuarios_Click(object sender, EventArgs e)
        {
            AbrirFormularios<FrmListadoUsuarios>();
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void AbrirFormularios<FormularioAbrir>() where FormularioAbrir:Form, new()
        {
            Form Formularios;

            Formularios = PanelContenedor.Controls.OfType<FormularioAbrir>().FirstOrDefault();

            if (Formularios == null)
            {
                Formularios = new FormularioAbrir
                {
                    TopLevel = false,
                    Dock = DockStyle.Fill
                };

                PanelContenedor.Controls.Add(Formularios);

                PanelContenedor.Tag = Formularios;

                Formularios.Show();

                Formularios.BringToFront();

            }
            else
            {
                Formularios.BringToFront();
            }

           
        }



    }
}
