﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmInicioSesion : Form
    {
        

        public FrmInicioSesion()
        {
            InitializeComponent();
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Btn_Editar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

      
        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            if (TxtUsuario.Text != "")
            {
                if (TxtContraseña.Text != "")
                {
                    N_InicioSesion inicioSesion = new N_InicioSesion();

                    var ValidarDatos = inicioSesion.InicioSesion(TxtUsuario.Text, TxtContraseña.Text);

                    if (ValidarDatos == true)
                    {
                        if (E_InicioSesion.Acceso == E_Acceso.Administrador)
                        {
                            Hide();
                            FrmMenu menu = new FrmMenu("Administrador");

                            
                            menu.Show();
                        }

                        if (E_InicioSesion.Acceso == E_Acceso.Cajero)
                        {
                            Hide();
                            FrmMenu menu = new FrmMenu("Cajero");
                            
                            menu.Show();
                        }
                    }
                    else
                    {
                        MensajeError("Datos de Usuarios incorrectos. Intente de nuevo");
                        TxtContraseña.Text = "";
                        TxtUsuario.Focus();
                    }

                }
                else
                {
                    MensajeError("Por favor ingrese su CONTRASEÑA");
                    TxtContraseña.Focus();
                }
            }
            else
            {
                MensajeError("Por favor ingrese su nombre de USUARIO");
                TxtUsuario.Focus();
            }
        }
    }
}
