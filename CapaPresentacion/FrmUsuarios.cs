﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmUsuarios : Form
    {
        readonly E_Usuarios ObjEntidad = new E_Usuarios();
        readonly N_Usuarios ObjNegocios = new N_Usuarios();

        public FrmUsuarios()
        {
            InitializeComponent();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void LimpiarCajas()
        {
            TxtCodigo.Text = "";
            TxtUsuario.Text = "";
            TxtContraseña.Text = "";
            CbAcceso.Text = "";

            TxtUsuario.Focus();
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            if (TxtUsuario.Text.Trim() != "")
            {
                if (Program.Evento == 0)
                {
                    try
                    {
                        ObjEntidad.Usuarios = TxtUsuario.Text;
                        ObjEntidad.Contraseña = TxtContraseña.Text;
                        ObjEntidad.Acceso = CbAcceso.Text;

                        ObjNegocios.InsertarRegistros(ObjEntidad);
                        MensajeConfirmacion("Se insertó correctamente");
                        Program.Evento = 0;
                        LimpiarCajas();
                        Close();

                    }
                    catch (Exception)
                    {
                        MensajeError("No se pudo guardar el registro");
                    }
                }
                else
                {
                    try
                    {
                        ObjEntidad.IdUsuarios = Convert.ToInt32(TxtCodigo.Text);
                        ObjEntidad.Usuarios = TxtUsuario.Text;
                        ObjEntidad.Contraseña = TxtContraseña.Text;
                        ObjEntidad.Acceso = CbAcceso.Text;

                        ObjNegocios.EditarRegistros(ObjEntidad);
                        MensajeConfirmacion("Se modificó correctamente");
                        Program.Evento = 0;
                        LimpiarCajas();
                        Close();

                    }
                    catch (Exception)
                    {
                        MensajeError("No se pudo editar el registro");
                    }
                }
            }
            else
            {
                MensajeError("Llene los campos correspondientes para guardar el registro");
            }
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            LimpiarCajas();
        }
    }
}
