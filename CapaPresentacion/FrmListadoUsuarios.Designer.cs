﻿namespace CapaPresentacion
{
    partial class FrmListadoUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmListadoUsuarios));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Pb_Cerrar = new System.Windows.Forms.PictureBox();
            this.Data_usuarios = new System.Windows.Forms.DataGridView();
            this.Btn_Eliminar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Editar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Btn_Nuevo = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pb_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Data_usuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 8;
            this.bunifuElipse1.TargetControl = this;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 70);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(102, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 21);
            this.label1.TabIndex = 39;
            this.label1.Text = ".::USUARIOS::.";
            // 
            // Pb_Cerrar
            // 
            this.Pb_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Pb_Cerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Pb_Cerrar.Image = ((System.Drawing.Image)(resources.GetObject("Pb_Cerrar.Image")));
            this.Pb_Cerrar.Location = new System.Drawing.Point(625, 12);
            this.Pb_Cerrar.Name = "Pb_Cerrar";
            this.Pb_Cerrar.Size = new System.Drawing.Size(40, 40);
            this.Pb_Cerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Pb_Cerrar.TabIndex = 38;
            this.Pb_Cerrar.TabStop = false;
            this.Pb_Cerrar.Click += new System.EventHandler(this.Pb_Cerrar_Click);
            // 
            // Data_usuarios
            // 
            this.Data_usuarios.AllowUserToAddRows = false;
            this.Data_usuarios.AllowUserToDeleteRows = false;
            this.Data_usuarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Data_usuarios.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(61)))), ((int)(((byte)(69)))));
            this.Data_usuarios.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Data_usuarios.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(61)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Data_usuarios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.Data_usuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Data_usuarios.EnableHeadersVisualStyles = false;
            this.Data_usuarios.GridColor = System.Drawing.Color.SteelBlue;
            this.Data_usuarios.Location = new System.Drawing.Point(40, 92);
            this.Data_usuarios.Name = "Data_usuarios";
            this.Data_usuarios.ReadOnly = true;
            this.Data_usuarios.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(61)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Data_usuarios.RowHeadersDefaultCellStyle = dataGridViewCellStyle26;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(61)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.White;
            this.Data_usuarios.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.Data_usuarios.Size = new System.Drawing.Size(430, 240);
            this.Data_usuarios.TabIndex = 37;
            // 
            // Btn_Eliminar
            // 
            this.Btn_Eliminar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Eliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Eliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Eliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Eliminar.BorderRadius = 5;
            this.Btn_Eliminar.ButtonText = "ELIMINAR";
            this.Btn_Eliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Eliminar.DisabledColor = System.Drawing.Color.Gray;
            this.Btn_Eliminar.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Eliminar.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Eliminar.Iconimage")));
            this.Btn_Eliminar.Iconimage_right = null;
            this.Btn_Eliminar.Iconimage_right_Selected = null;
            this.Btn_Eliminar.Iconimage_Selected = null;
            this.Btn_Eliminar.IconMarginLeft = 0;
            this.Btn_Eliminar.IconMarginRight = 0;
            this.Btn_Eliminar.IconRightVisible = true;
            this.Btn_Eliminar.IconRightZoom = 0D;
            this.Btn_Eliminar.IconVisible = true;
            this.Btn_Eliminar.IconZoom = 50D;
            this.Btn_Eliminar.IsTab = false;
            this.Btn_Eliminar.Location = new System.Drawing.Point(499, 207);
            this.Btn_Eliminar.Name = "Btn_Eliminar";
            this.Btn_Eliminar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Eliminar.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Btn_Eliminar.OnHoverTextColor = System.Drawing.Color.White;
            this.Btn_Eliminar.selected = false;
            this.Btn_Eliminar.Size = new System.Drawing.Size(150, 40);
            this.Btn_Eliminar.TabIndex = 35;
            this.Btn_Eliminar.Text = "ELIMINAR";
            this.Btn_Eliminar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Eliminar.Textcolor = System.Drawing.Color.White;
            this.Btn_Eliminar.TextFont = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Eliminar.Click += new System.EventHandler(this.Btn_Eliminar_Click);
            // 
            // Btn_Editar
            // 
            this.Btn_Editar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Editar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Editar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Editar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Editar.BorderRadius = 5;
            this.Btn_Editar.ButtonText = "EDITAR";
            this.Btn_Editar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Editar.DisabledColor = System.Drawing.Color.Gray;
            this.Btn_Editar.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Editar.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Editar.Iconimage")));
            this.Btn_Editar.Iconimage_right = null;
            this.Btn_Editar.Iconimage_right_Selected = null;
            this.Btn_Editar.Iconimage_Selected = null;
            this.Btn_Editar.IconMarginLeft = 0;
            this.Btn_Editar.IconMarginRight = 0;
            this.Btn_Editar.IconRightVisible = true;
            this.Btn_Editar.IconRightZoom = 0D;
            this.Btn_Editar.IconVisible = true;
            this.Btn_Editar.IconZoom = 50D;
            this.Btn_Editar.IsTab = false;
            this.Btn_Editar.Location = new System.Drawing.Point(499, 148);
            this.Btn_Editar.Name = "Btn_Editar";
            this.Btn_Editar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Editar.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Btn_Editar.OnHoverTextColor = System.Drawing.Color.White;
            this.Btn_Editar.selected = false;
            this.Btn_Editar.Size = new System.Drawing.Size(150, 40);
            this.Btn_Editar.TabIndex = 34;
            this.Btn_Editar.Text = "EDITAR";
            this.Btn_Editar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Editar.Textcolor = System.Drawing.Color.White;
            this.Btn_Editar.TextFont = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Editar.Click += new System.EventHandler(this.Btn_Editar_Click);
            // 
            // Btn_Nuevo
            // 
            this.Btn_Nuevo.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Nuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Nuevo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Nuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_Nuevo.BorderRadius = 5;
            this.Btn_Nuevo.ButtonText = "NUEVO";
            this.Btn_Nuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Nuevo.DisabledColor = System.Drawing.Color.Gray;
            this.Btn_Nuevo.Iconcolor = System.Drawing.Color.Transparent;
            this.Btn_Nuevo.Iconimage = ((System.Drawing.Image)(resources.GetObject("Btn_Nuevo.Iconimage")));
            this.Btn_Nuevo.Iconimage_right = null;
            this.Btn_Nuevo.Iconimage_right_Selected = null;
            this.Btn_Nuevo.Iconimage_Selected = null;
            this.Btn_Nuevo.IconMarginLeft = 0;
            this.Btn_Nuevo.IconMarginRight = 0;
            this.Btn_Nuevo.IconRightVisible = true;
            this.Btn_Nuevo.IconRightZoom = 0D;
            this.Btn_Nuevo.IconVisible = true;
            this.Btn_Nuevo.IconZoom = 50D;
            this.Btn_Nuevo.IsTab = false;
            this.Btn_Nuevo.Location = new System.Drawing.Point(499, 92);
            this.Btn_Nuevo.Name = "Btn_Nuevo";
            this.Btn_Nuevo.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(45)))), ((int)(((byte)(53)))));
            this.Btn_Nuevo.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Btn_Nuevo.OnHoverTextColor = System.Drawing.Color.White;
            this.Btn_Nuevo.selected = false;
            this.Btn_Nuevo.Size = new System.Drawing.Size(150, 40);
            this.Btn_Nuevo.TabIndex = 33;
            this.Btn_Nuevo.Text = "NUEVO";
            this.Btn_Nuevo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Btn_Nuevo.Textcolor = System.Drawing.Color.White;
            this.Btn_Nuevo.TextFont = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Nuevo.Click += new System.EventHandler(this.Btn_Nuevo_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.label1;
            this.bunifuDragControl1.Vertical = true;
            // 
            // FrmListadoUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.ClientSize = new System.Drawing.Size(677, 380);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pb_Cerrar);
            this.Controls.Add(this.Data_usuarios);
            this.Controls.Add(this.Btn_Eliminar);
            this.Controls.Add(this.Btn_Editar);
            this.Controls.Add(this.Btn_Nuevo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmListadoUsuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmListadoUsuarios";
            this.Load += new System.EventHandler(this.FrmListadoUsuarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pb_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Data_usuarios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Pb_Cerrar;
        private System.Windows.Forms.DataGridView Data_usuarios;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Eliminar;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Editar;
        private Bunifu.Framework.UI.BunifuFlatButton Btn_Nuevo;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
    }
}