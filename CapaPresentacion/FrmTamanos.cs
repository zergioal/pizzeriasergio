﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmTamanos : Form
    {
        private bool editar = false;
        readonly E_Tamanos ObjEntidad = new E_Tamanos();
        readonly N_Tamanos ObjNegocio = new N_Tamanos();

        public FrmTamanos()
        {
            InitializeComponent();
        }

        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void AccionesTabla()
        {
            Data_tamanos.Columns[0].HeaderText = "Código";
            Data_tamanos.Columns[1].HeaderText = "Descripción";
        }

        private void MostrarRegistros()
        {
            Data_tamanos.DataSource = N_Tamanos.MostrarRegistros();
            AccionesTabla();
        }

        private void LimpiarCajas()
        {
            editar = false;
            Txt_Codigo.Text = "";
            Txt_Descripcion.Text = "";
            Txt_Descripcion.Focus();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmTamanos_Load(object sender, EventArgs e)
        {
            MostrarRegistros();
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            editar = false;
            LimpiarCajas();
        }

        private void Btn_Editar_Click(object sender, EventArgs e)
        {
            if(Data_tamanos.SelectedRows.Count>0)
            {
                editar = true;
                Txt_Codigo.Text = Data_tamanos.CurrentRow.Cells[0].Value.ToString();
                Txt_Descripcion.Text = Data_tamanos.CurrentRow.Cells[1].Value.ToString();
            }
            else
            {
                MensajeError("Seleccione una fila primero");
            }
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (Data_tamanos.SelectedRows.Count>0)
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Realmente desea eliminar el registro?", "Pizzería Zizu", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion==DialogResult.OK)
                {
                    ObjEntidad.IdTamano = Convert.ToInt32(Data_tamanos.CurrentRow.Cells[0].Value.ToString());
                    ObjNegocio.EliminarRegistros(ObjEntidad);

                    MensajeConfirmacion("Se eliminó correctamente el registro");

                    MostrarRegistros();
                }
            }
            else
            {
                MensajeError("Seleccione primero una fila a eliminar");
            }
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            if(editar==false)
            {
                try
                {
                    ObjEntidad.Descripcion = Txt_Descripcion.Text.ToUpper();

                    ObjNegocio.InsertarRegistros(ObjEntidad);

                    MensajeConfirmacion("Se insertó correctamente el registro");
                    MostrarRegistros();
                    LimpiarCajas();
                }
                catch (Exception)
                {
                    MensajeError("No se pudo insertar el registro");
                }
            }

            if(editar==true)
            {
                try
                {
                    ObjEntidad.IdTamano = Convert.ToInt32(Txt_Codigo.Text);
                    ObjEntidad.Descripcion = Txt_Descripcion.Text.ToUpper();

                    ObjNegocio.EditarRegistros(ObjEntidad);

                    MensajeConfirmacion("Se modificó correctamente el registro");
                    MostrarRegistros();
                    LimpiarCajas();
                }
                catch (Exception)
                {
                    MensajeError("No se pudo modificar el registro");
                }
            }
        }
    }
}
