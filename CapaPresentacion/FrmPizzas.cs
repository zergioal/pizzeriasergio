﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmPizzas : Form
    {
        readonly E_Pizzas ObjEntidad = new E_Pizzas();
        readonly N_Pizzas ObjNegocios = new N_Pizzas();

        public FrmPizzas()
        {
            InitializeComponent();
            LlenarComboBox();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void LimpiarCajas()
        {
            TxtCodigo.Text = "";
            TxtNombre.Text = "";
            TxtCaracteristicas.Text = "";
            TxtPrecioVenta.Text = "";
            TxtCostoProduccion.Text = "";
            CbTamano.Text = "";

            TxtNombre.Focus();
        }

        private void LlenarComboBox()
        {
            CbTamano.DataSource = N_Tamanos.MostrarRegistros();
            CbTamano.ValueMember = "idtamano";
            CbTamano.DisplayMember = "descripcion";

        }

        private void ActualizarDatos(object sender, FormClosedEventArgs e)
        {
            LlenarComboBox();
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            if(TxtNombre.Text.Trim() != "")
            {
                if (Program.Evento == 0)
                {
                    try
                    {
                        ObjEntidad.Nombre = TxtNombre.Text;
                        ObjEntidad.Caracteristicas = TxtCaracteristicas.Text;
                        ObjEntidad.PrecioVenta = Convert.ToDecimal(TxtPrecioVenta.Text);
                        ObjEntidad.CostoProduccion = Convert.ToDecimal(TxtCostoProduccion.Text);
                        ObjEntidad.IdTamano = Convert.ToInt32(CbTamano.SelectedValue);

                        ObjNegocios.InsertarRegistros(ObjEntidad);
                        MensajeConfirmacion("Se insertó correctamente");
                        Program.Evento = 0;
                        LimpiarCajas();
                        Close();

                    }
                    catch (Exception)
                    {

                        MensajeError("No se pudo guardar el registro");
                    }
                }
                else
                {
                    try
                    {
                        ObjEntidad.IdPizzas = Convert.ToInt32(TxtCodigo.Text);
                        ObjEntidad.Nombre = TxtNombre.Text;
                        ObjEntidad.Caracteristicas = TxtCaracteristicas.Text;
                        ObjEntidad.PrecioVenta = Convert.ToDecimal(TxtPrecioVenta.Text);
                        ObjEntidad.CostoProduccion = Convert.ToDecimal(TxtCostoProduccion.Text);
                        ObjEntidad.IdTamano = Convert.ToInt32(CbTamano.SelectedValue);

                        ObjNegocios.EditarRegistros(ObjEntidad);
                        MensajeConfirmacion("Se modificó correctamente");
                        Program.Evento = 0;
                        LimpiarCajas();
                        Close();

                    }
                    catch (Exception)
                    {
                        MensajeError("No se pudo editar el registro");
                    }
                }
            }
            else
            {
                MensajeError("Llene los campos correspondientes para guardar el registro");
            }
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            LimpiarCajas();
        }

        private void BtnTamanos_Click(object sender, EventArgs e)
        {
            FrmTamanos nuevoregistro = new FrmTamanos();
            nuevoregistro.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            nuevoregistro.ShowDialog();
        }
    }
}
