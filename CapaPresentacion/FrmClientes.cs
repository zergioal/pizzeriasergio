﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmClientes : Form
    {
        readonly E_Clientes ObjEntidad = new E_Clientes();
        readonly N_Clientes ObjNegocios = new N_Clientes();

        public FrmClientes()
        {
            InitializeComponent();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void LimpiarCajas()
        {
            TxtCodigo.Text = "";
            TxtNombre.Text = "";
            TxtApellido.Text = "";
            TxtCiNit.Text = "";
            TxtDireccion.Text = "";
            TxtTelefono.Text = "";

            TxtCiNit.Focus();
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            if (TxtNombre.Text.Trim() != "")
            {
                if (Program.Evento == 0)
                {
                    try
                    {
                        ObjEntidad.Nombre = TxtNombre.Text;
                        ObjEntidad.Apellido = TxtApellido.Text;
                        ObjEntidad.CioNit = TxtCiNit.Text;
                        ObjEntidad.Direccion = TxtDireccion.Text;
                        ObjEntidad.Telefono = TxtTelefono.Text;

                        ObjNegocios.InsertarRegistros(ObjEntidad);
                        MensajeConfirmacion("Se insertó correctamente");
                        Program.Evento = 0;
                        LimpiarCajas();
                        Close();

                    }
                    catch (Exception)
                    {

                        MensajeError("No se pudo guardar el registro");
                    }
                }
                else
                {
                    try
                    {
                        ObjEntidad.IdClientes = Convert.ToInt32(TxtCodigo.Text);
                        ObjEntidad.Nombre = TxtNombre.Text;
                        ObjEntidad.Apellido = TxtApellido.Text;
                        ObjEntidad.CioNit = TxtCiNit.Text;
                        ObjEntidad.Direccion = TxtDireccion.Text;
                        ObjEntidad.Telefono = TxtTelefono.Text;

                        ObjNegocios.EditarRegistros(ObjEntidad);
                        MensajeConfirmacion("Se modificó correctamente");
                        Program.Evento = 0;
                        LimpiarCajas();
                        Close();

                    }
                    catch (Exception)
                    {
                        MensajeError("No se pudo editar el registro");
                    }
                }
            }
            else
            {
                MensajeError("Llene los campos correspondientes para guardar el registro");
            }
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            LimpiarCajas();
        }
    }
}
