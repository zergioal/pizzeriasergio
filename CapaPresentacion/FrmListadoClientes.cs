﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmListadoClientes : Form
    {
        readonly E_Clientes ObjEntidad = new E_Clientes();
        readonly N_Clientes ObjNegocio = new N_Clientes();
        public FrmListadoClientes()
        {
            InitializeComponent();
        }

        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void AccionesTabla()
        {
            Data_clientes.Columns[0].Visible = false;

            Data_clientes.Columns[1].HeaderText = "Nombre";
            Data_clientes.Columns[2].HeaderText = "Apellido";
            Data_clientes.Columns[3].HeaderText = "Carnet o NIT";
            Data_clientes.Columns[4].HeaderText = "Dirección";
            Data_clientes.Columns[5].HeaderText = "Teléfono";

        }

        private void MostrarRegistros()
        {
            Data_clientes.DataSource = N_Clientes.MostrarRegistros();
            AccionesTabla();
        }

        public void BuscarRegistros()
        {
            Data_clientes.DataSource = N_Clientes.BuscarRegistros(TxtBuscar.Text);
        }

        private void ActualizarDatos(object sender, FormClosedEventArgs e)
        {
            MostrarRegistros();
            Data_clientes.ClearSelection();
        }

        public void ExportarDatos(DataGridView datalistado)
        {
            Microsoft.Office.Interop.Excel.Application exportarExcel = new Microsoft.Office.Interop.Excel.Application();
            exportarExcel.Application.Workbooks.Add(true);

            int indicecolumna = 0;

            foreach (DataGridViewColumn columna in datalistado.Columns)
            {
                indicecolumna++;

                exportarExcel.Cells[1, indicecolumna] = columna.Name;
            }

            int indicefila = 0;

            foreach (DataGridViewRow fila in datalistado.Rows)
            {
                indicefila++;
                indicecolumna = 0;

                foreach (DataGridViewColumn columna in datalistado.Columns)
                {
                    indicecolumna++;
                    exportarExcel.Cells[indicefila + 1, indicecolumna] = fila.Cells[columna.Name].Value;
                }
            }

            exportarExcel.Visible = true;
        }

        private void FrmListadoClientes_Load(object sender, EventArgs e)
        {
            MostrarRegistros();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            Program.Evento = 0;
            FrmClientes nuevoregistro = new FrmClientes();
            nuevoregistro.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            nuevoregistro.ShowDialog();
        }

        private void Btn_Editar_Click(object sender, EventArgs e)
        {
            FrmClientes editarregistros = new FrmClientes();
            editarregistros.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            if (Data_clientes.SelectedRows.Count > 0)
            {
                Program.Evento = 1;
                editarregistros.TxtCodigo.Text = Data_clientes.CurrentRow.Cells[0].Value.ToString();
                editarregistros.TxtNombre.Text = Data_clientes.CurrentRow.Cells[1].Value.ToString();
                editarregistros.TxtApellido.Text = Data_clientes.CurrentRow.Cells[2].Value.ToString();
                editarregistros.TxtCiNit.Text = Data_clientes.CurrentRow.Cells[3].Value.ToString();
                editarregistros.TxtDireccion.Text = Data_clientes.CurrentRow.Cells[4].Value.ToString();
                editarregistros.TxtTelefono.Text = Data_clientes.CurrentRow.Cells[5].Value.ToString();
                editarregistros.ShowDialog();
            }
            else
            {
                MensajeError("Selecciona la fila a editar");
            }
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (Data_clientes.SelectedRows.Count > 0)
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Realmente quiere eliminar el registro?", "Pizzeria Zizu", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion == DialogResult.OK)
                {
                    ObjEntidad.IdClientes = Convert.ToInt32(Data_clientes.CurrentRow.Cells[0].Value.ToString());

                    ObjNegocio.EliminarRegistros(ObjEntidad);

                    MensajeConfirmacion("Se eliminó correctamente");
                    MostrarRegistros();
                }

            }
            else
            {
                MensajeError("Seleccione un registro a eliminar");
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarRegistros();
        }

        private void Btn_Exportar_Click(object sender, EventArgs e)
        {
            ExportarDatos(Data_clientes);
        }
    }
}
