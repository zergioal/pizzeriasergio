﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmListadoUsuarios : Form
    {
        readonly E_Usuarios ObjEntidad = new E_Usuarios();
        readonly N_Usuarios ObjNegocio = new N_Usuarios();
        public FrmListadoUsuarios()
        {
            InitializeComponent();
        }

        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void AccionesTabla()
        {
            Data_usuarios.Columns[0].Visible = false;
            Data_usuarios.Columns[2].Visible = false;

            Data_usuarios.Columns[1].HeaderText = "Usuario";
            Data_usuarios.Columns[3].HeaderText = "Acceso";   
        }

        private void MostrarRegistros()
        {
            Data_usuarios.DataSource = ObjNegocio.ListarUsuarios();
            AccionesTabla();
        }

        private void ActualizarDatos(object sender, FormClosedEventArgs e)
        {
            MostrarRegistros();
            Data_usuarios.ClearSelection();
        }

        private void FrmListadoUsuarios_Load(object sender, EventArgs e)
        {
            MostrarRegistros();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            Program.Evento = 0;
            FrmUsuarios nuevoregistro = new FrmUsuarios();
            nuevoregistro.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            nuevoregistro.ShowDialog();
        }

        private void Btn_Editar_Click(object sender, EventArgs e)
        {
            FrmUsuarios editarregistros = new FrmUsuarios();
            editarregistros.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            if (Data_usuarios.SelectedRows.Count > 0)
            {
                Program.Evento = 1;
                editarregistros.TxtCodigo.Text = Data_usuarios.CurrentRow.Cells[0].Value.ToString();
                editarregistros.TxtUsuario.Text = Data_usuarios.CurrentRow.Cells[1].Value.ToString();
                editarregistros.TxtContraseña.Text = Data_usuarios.CurrentRow.Cells[2].Value.ToString();
                editarregistros.CbAcceso.Text = Data_usuarios.CurrentRow.Cells[3].Value.ToString();
                editarregistros.ShowDialog();
            }
            else
            {
                MensajeError("Selecciona la fila a editar");
            }
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (Data_usuarios.SelectedRows.Count > 0)
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Realmente quiere eliminar el registro?", "Pizzeria Zizu", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (opcion == DialogResult.OK)
                {
                    ObjEntidad.IdUsuarios = Convert.ToInt32(Data_usuarios.CurrentRow.Cells[0].Value.ToString());

                    ObjNegocio.EliminarRegistros(ObjEntidad);

                    MensajeConfirmacion("Se eliminó correctamente");
                    MostrarRegistros();
                }

            }
            else
            { 
                MensajeError("Seleccione un registro a eliminar");
            }
        }
    }
}
