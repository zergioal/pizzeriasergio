﻿namespace CapaPresentacion
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.PanelPrincipal = new System.Windows.Forms.Panel();
            this.PanelSuperior = new System.Windows.Forms.Panel();
            this.BtnRestaurar = new Bunifu.Framework.UI.BunifuImageButton();
            this.BtnMaximizar = new Bunifu.Framework.UI.BunifuImageButton();
            this.BtnMinimizar = new Bunifu.Framework.UI.BunifuImageButton();
            this.BtnCerrar = new Bunifu.Framework.UI.BunifuImageButton();
            this.PanelContenedor = new System.Windows.Forms.Panel();
            this.PanelInferior = new System.Windows.Forms.Panel();
            this.PanelSidebar = new System.Windows.Forms.Panel();
            this.BtnUsuarios = new Bunifu.Framework.UI.BunifuFlatButton();
            this.BtnTamanos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.BtnPizzas = new Bunifu.Framework.UI.BunifuFlatButton();
            this.BtnClientes = new Bunifu.Framework.UI.BunifuFlatButton();
            this.BtnDashboard = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.BtnEditarPerfil = new Bunifu.Framework.UI.BunifuThinButton2();
            this.LblUsuario = new System.Windows.Forms.Label();
            this.LblAcceso = new System.Windows.Forms.Label();
            this.bunifuImageButton5 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuElipse2 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.PanelPrincipal.SuspendLayout();
            this.PanelSuperior.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnRestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnCerrar)).BeginInit();
            this.PanelSidebar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelPrincipal
            // 
            this.PanelPrincipal.Controls.Add(this.PanelSuperior);
            this.PanelPrincipal.Controls.Add(this.PanelContenedor);
            this.PanelPrincipal.Controls.Add(this.PanelInferior);
            this.PanelPrincipal.Controls.Add(this.PanelSidebar);
            this.PanelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPrincipal.Location = new System.Drawing.Point(0, 0);
            this.PanelPrincipal.Name = "PanelPrincipal";
            this.PanelPrincipal.Size = new System.Drawing.Size(1200, 700);
            this.PanelPrincipal.TabIndex = 0;
            // 
            // PanelSuperior
            // 
            this.PanelSuperior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.PanelSuperior.Controls.Add(this.BtnRestaurar);
            this.PanelSuperior.Controls.Add(this.BtnMaximizar);
            this.PanelSuperior.Controls.Add(this.BtnMinimizar);
            this.PanelSuperior.Controls.Add(this.BtnCerrar);
            this.PanelSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelSuperior.Location = new System.Drawing.Point(230, 0);
            this.PanelSuperior.Name = "PanelSuperior";
            this.PanelSuperior.Size = new System.Drawing.Size(970, 80);
            this.PanelSuperior.TabIndex = 1;
            // 
            // BtnRestaurar
            // 
            this.BtnRestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnRestaurar.BackColor = System.Drawing.Color.Transparent;
            this.BtnRestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnRestaurar.Image = ((System.Drawing.Image)(resources.GetObject("BtnRestaurar.Image")));
            this.BtnRestaurar.ImageActive = null;
            this.BtnRestaurar.Location = new System.Drawing.Point(872, 12);
            this.BtnRestaurar.Name = "BtnRestaurar";
            this.BtnRestaurar.Size = new System.Drawing.Size(40, 40);
            this.BtnRestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnRestaurar.TabIndex = 3;
            this.BtnRestaurar.TabStop = false;
            this.BtnRestaurar.Visible = false;
            this.BtnRestaurar.Zoom = 10;
            this.BtnRestaurar.Click += new System.EventHandler(this.BtnRestaurar_Click);
            // 
            // BtnMaximizar
            // 
            this.BtnMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnMaximizar.BackColor = System.Drawing.Color.Transparent;
            this.BtnMaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMaximizar.Image = ((System.Drawing.Image)(resources.GetObject("BtnMaximizar.Image")));
            this.BtnMaximizar.ImageActive = null;
            this.BtnMaximizar.Location = new System.Drawing.Point(872, 12);
            this.BtnMaximizar.Name = "BtnMaximizar";
            this.BtnMaximizar.Size = new System.Drawing.Size(40, 40);
            this.BtnMaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnMaximizar.TabIndex = 2;
            this.BtnMaximizar.TabStop = false;
            this.BtnMaximizar.Zoom = 10;
            this.BtnMaximizar.Click += new System.EventHandler(this.BtnMaximizar_Click);
            // 
            // BtnMinimizar
            // 
            this.BtnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.BtnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("BtnMinimizar.Image")));
            this.BtnMinimizar.ImageActive = null;
            this.BtnMinimizar.Location = new System.Drawing.Point(826, 12);
            this.BtnMinimizar.Name = "BtnMinimizar";
            this.BtnMinimizar.Size = new System.Drawing.Size(40, 40);
            this.BtnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnMinimizar.TabIndex = 1;
            this.BtnMinimizar.TabStop = false;
            this.BtnMinimizar.Zoom = 10;
            this.BtnMinimizar.Click += new System.EventHandler(this.BtnMinimizar_Click);
            // 
            // BtnCerrar
            // 
            this.BtnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCerrar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCerrar.Image")));
            this.BtnCerrar.ImageActive = null;
            this.BtnCerrar.Location = new System.Drawing.Point(918, 12);
            this.BtnCerrar.Name = "BtnCerrar";
            this.BtnCerrar.Size = new System.Drawing.Size(40, 40);
            this.BtnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnCerrar.TabIndex = 0;
            this.BtnCerrar.TabStop = false;
            this.BtnCerrar.Zoom = 10;
            this.BtnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelContenedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.PanelContenedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelContenedor.BackgroundImage")));
            this.PanelContenedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PanelContenedor.Location = new System.Drawing.Point(235, 87);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(960, 508);
            this.PanelContenedor.TabIndex = 3;
            // 
            // PanelInferior
            // 
            this.PanelInferior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(61)))), ((int)(((byte)(69)))));
            this.PanelInferior.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelInferior.Location = new System.Drawing.Point(230, 600);
            this.PanelInferior.Name = "PanelInferior";
            this.PanelInferior.Size = new System.Drawing.Size(970, 100);
            this.PanelInferior.TabIndex = 2;
            // 
            // PanelSidebar
            // 
            this.PanelSidebar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.PanelSidebar.Controls.Add(this.BtnUsuarios);
            this.PanelSidebar.Controls.Add(this.BtnTamanos);
            this.PanelSidebar.Controls.Add(this.BtnPizzas);
            this.PanelSidebar.Controls.Add(this.BtnClientes);
            this.PanelSidebar.Controls.Add(this.BtnDashboard);
            this.PanelSidebar.Controls.Add(this.bunifuSeparator1);
            this.PanelSidebar.Controls.Add(this.BtnEditarPerfil);
            this.PanelSidebar.Controls.Add(this.LblUsuario);
            this.PanelSidebar.Controls.Add(this.LblAcceso);
            this.PanelSidebar.Controls.Add(this.bunifuImageButton5);
            this.PanelSidebar.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelSidebar.Location = new System.Drawing.Point(0, 0);
            this.PanelSidebar.Name = "PanelSidebar";
            this.PanelSidebar.Size = new System.Drawing.Size(230, 700);
            this.PanelSidebar.TabIndex = 0;
            // 
            // BtnUsuarios
            // 
            this.BtnUsuarios.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnUsuarios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnUsuarios.BorderRadius = 7;
            this.BtnUsuarios.ButtonText = "USUARIOS";
            this.BtnUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnUsuarios.DisabledColor = System.Drawing.Color.Gray;
            this.BtnUsuarios.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUsuarios.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnUsuarios.Iconimage = ((System.Drawing.Image)(resources.GetObject("BtnUsuarios.Iconimage")));
            this.BtnUsuarios.Iconimage_right = null;
            this.BtnUsuarios.Iconimage_right_Selected = null;
            this.BtnUsuarios.Iconimage_Selected = null;
            this.BtnUsuarios.IconMarginLeft = 0;
            this.BtnUsuarios.IconMarginRight = 0;
            this.BtnUsuarios.IconRightVisible = true;
            this.BtnUsuarios.IconRightZoom = 0D;
            this.BtnUsuarios.IconVisible = true;
            this.BtnUsuarios.IconZoom = 70D;
            this.BtnUsuarios.IsTab = false;
            this.BtnUsuarios.Location = new System.Drawing.Point(37, 431);
            this.BtnUsuarios.Margin = new System.Windows.Forms.Padding(4);
            this.BtnUsuarios.Name = "BtnUsuarios";
            this.BtnUsuarios.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnUsuarios.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnUsuarios.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnUsuarios.selected = false;
            this.BtnUsuarios.Size = new System.Drawing.Size(171, 55);
            this.BtnUsuarios.TabIndex = 14;
            this.BtnUsuarios.Text = "USUARIOS";
            this.BtnUsuarios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnUsuarios.Textcolor = System.Drawing.Color.White;
            this.BtnUsuarios.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUsuarios.Click += new System.EventHandler(this.BtnUsuarios_Click);
            // 
            // BtnTamanos
            // 
            this.BtnTamanos.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnTamanos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnTamanos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnTamanos.BorderRadius = 7;
            this.BtnTamanos.ButtonText = "TAMAÑOS";
            this.BtnTamanos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnTamanos.DisabledColor = System.Drawing.Color.Gray;
            this.BtnTamanos.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTamanos.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnTamanos.Iconimage = ((System.Drawing.Image)(resources.GetObject("BtnTamanos.Iconimage")));
            this.BtnTamanos.Iconimage_right = null;
            this.BtnTamanos.Iconimage_right_Selected = null;
            this.BtnTamanos.Iconimage_Selected = null;
            this.BtnTamanos.IconMarginLeft = 0;
            this.BtnTamanos.IconMarginRight = 0;
            this.BtnTamanos.IconRightVisible = true;
            this.BtnTamanos.IconRightZoom = 0D;
            this.BtnTamanos.IconVisible = true;
            this.BtnTamanos.IconZoom = 90D;
            this.BtnTamanos.IsTab = false;
            this.BtnTamanos.Location = new System.Drawing.Point(37, 368);
            this.BtnTamanos.Margin = new System.Windows.Forms.Padding(4);
            this.BtnTamanos.Name = "BtnTamanos";
            this.BtnTamanos.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnTamanos.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnTamanos.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnTamanos.selected = false;
            this.BtnTamanos.Size = new System.Drawing.Size(171, 55);
            this.BtnTamanos.TabIndex = 13;
            this.BtnTamanos.Text = "TAMAÑOS";
            this.BtnTamanos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnTamanos.Textcolor = System.Drawing.Color.White;
            this.BtnTamanos.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTamanos.Click += new System.EventHandler(this.BtnTamanos_Click);
            // 
            // BtnPizzas
            // 
            this.BtnPizzas.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnPizzas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnPizzas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnPizzas.BorderRadius = 7;
            this.BtnPizzas.ButtonText = "PIZZAS";
            this.BtnPizzas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnPizzas.DisabledColor = System.Drawing.Color.Gray;
            this.BtnPizzas.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPizzas.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnPizzas.Iconimage = ((System.Drawing.Image)(resources.GetObject("BtnPizzas.Iconimage")));
            this.BtnPizzas.Iconimage_right = null;
            this.BtnPizzas.Iconimage_right_Selected = null;
            this.BtnPizzas.Iconimage_Selected = null;
            this.BtnPizzas.IconMarginLeft = 0;
            this.BtnPizzas.IconMarginRight = 0;
            this.BtnPizzas.IconRightVisible = true;
            this.BtnPizzas.IconRightZoom = 0D;
            this.BtnPizzas.IconVisible = true;
            this.BtnPizzas.IconZoom = 90D;
            this.BtnPizzas.IsTab = false;
            this.BtnPizzas.Location = new System.Drawing.Point(37, 305);
            this.BtnPizzas.Margin = new System.Windows.Forms.Padding(4);
            this.BtnPizzas.Name = "BtnPizzas";
            this.BtnPizzas.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnPizzas.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnPizzas.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnPizzas.selected = false;
            this.BtnPizzas.Size = new System.Drawing.Size(171, 55);
            this.BtnPizzas.TabIndex = 12;
            this.BtnPizzas.Text = "PIZZAS";
            this.BtnPizzas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnPizzas.Textcolor = System.Drawing.Color.White;
            this.BtnPizzas.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPizzas.Click += new System.EventHandler(this.BtnPizzas_Click);
            // 
            // BtnClientes
            // 
            this.BtnClientes.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnClientes.BorderRadius = 7;
            this.BtnClientes.ButtonText = "CLIENTES";
            this.BtnClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnClientes.DisabledColor = System.Drawing.Color.Gray;
            this.BtnClientes.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClientes.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnClientes.Iconimage = ((System.Drawing.Image)(resources.GetObject("BtnClientes.Iconimage")));
            this.BtnClientes.Iconimage_right = null;
            this.BtnClientes.Iconimage_right_Selected = null;
            this.BtnClientes.Iconimage_Selected = null;
            this.BtnClientes.IconMarginLeft = 0;
            this.BtnClientes.IconMarginRight = 0;
            this.BtnClientes.IconRightVisible = true;
            this.BtnClientes.IconRightZoom = 0D;
            this.BtnClientes.IconVisible = true;
            this.BtnClientes.IconZoom = 90D;
            this.BtnClientes.IsTab = false;
            this.BtnClientes.Location = new System.Drawing.Point(37, 242);
            this.BtnClientes.Margin = new System.Windows.Forms.Padding(4);
            this.BtnClientes.Name = "BtnClientes";
            this.BtnClientes.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnClientes.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnClientes.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnClientes.selected = false;
            this.BtnClientes.Size = new System.Drawing.Size(171, 55);
            this.BtnClientes.TabIndex = 11;
            this.BtnClientes.Text = "CLIENTES";
            this.BtnClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnClientes.Textcolor = System.Drawing.Color.White;
            this.BtnClientes.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClientes.Click += new System.EventHandler(this.BtnClientes_Click);
            // 
            // BtnDashboard
            // 
            this.BtnDashboard.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnDashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnDashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnDashboard.BorderRadius = 7;
            this.BtnDashboard.ButtonText = "DASHBOARD";
            this.BtnDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnDashboard.DisabledColor = System.Drawing.Color.Gray;
            this.BtnDashboard.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDashboard.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnDashboard.Iconimage = ((System.Drawing.Image)(resources.GetObject("BtnDashboard.Iconimage")));
            this.BtnDashboard.Iconimage_right = null;
            this.BtnDashboard.Iconimage_right_Selected = null;
            this.BtnDashboard.Iconimage_Selected = null;
            this.BtnDashboard.IconMarginLeft = 0;
            this.BtnDashboard.IconMarginRight = 0;
            this.BtnDashboard.IconRightVisible = true;
            this.BtnDashboard.IconRightZoom = 0D;
            this.BtnDashboard.IconVisible = true;
            this.BtnDashboard.IconZoom = 90D;
            this.BtnDashboard.IsTab = false;
            this.BtnDashboard.Location = new System.Drawing.Point(37, 179);
            this.BtnDashboard.Margin = new System.Windows.Forms.Padding(4);
            this.BtnDashboard.Name = "BtnDashboard";
            this.BtnDashboard.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnDashboard.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(76)))));
            this.BtnDashboard.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnDashboard.selected = false;
            this.BtnDashboard.Size = new System.Drawing.Size(171, 55);
            this.BtnDashboard.TabIndex = 10;
            this.BtnDashboard.Text = "DASHBOARD";
            this.BtnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnDashboard.Textcolor = System.Drawing.Color.White;
            this.BtnDashboard.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(3, 137);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(224, 35);
            this.bunifuSeparator1.TabIndex = 9;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // BtnEditarPerfil
            // 
            this.BtnEditarPerfil.ActiveBorderThickness = 1;
            this.BtnEditarPerfil.ActiveCornerRadius = 20;
            this.BtnEditarPerfil.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.BtnEditarPerfil.ActiveForecolor = System.Drawing.Color.White;
            this.BtnEditarPerfil.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.BtnEditarPerfil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(34)))), ((int)(((byte)(39)))));
            this.BtnEditarPerfil.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnEditarPerfil.BackgroundImage")));
            this.BtnEditarPerfil.ButtonText = "Editar Perfil";
            this.BtnEditarPerfil.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEditarPerfil.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEditarPerfil.ForeColor = System.Drawing.Color.SeaGreen;
            this.BtnEditarPerfil.IdleBorderThickness = 1;
            this.BtnEditarPerfil.IdleCornerRadius = 20;
            this.BtnEditarPerfil.IdleFillColor = System.Drawing.Color.Transparent;
            this.BtnEditarPerfil.IdleForecolor = System.Drawing.Color.SeaShell;
            this.BtnEditarPerfil.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.BtnEditarPerfil.Location = new System.Drawing.Point(14, 98);
            this.BtnEditarPerfil.Margin = new System.Windows.Forms.Padding(5);
            this.BtnEditarPerfil.Name = "BtnEditarPerfil";
            this.BtnEditarPerfil.Size = new System.Drawing.Size(194, 41);
            this.BtnEditarPerfil.TabIndex = 8;
            this.BtnEditarPerfil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblUsuario
            // 
            this.LblUsuario.AutoSize = true;
            this.LblUsuario.ForeColor = System.Drawing.Color.White;
            this.LblUsuario.Location = new System.Drawing.Point(125, 57);
            this.LblUsuario.Name = "LblUsuario";
            this.LblUsuario.Size = new System.Drawing.Size(43, 13);
            this.LblUsuario.TabIndex = 6;
            this.LblUsuario.Text = "Usuario";
            // 
            // LblAcceso
            // 
            this.LblAcceso.AutoSize = true;
            this.LblAcceso.ForeColor = System.Drawing.Color.White;
            this.LblAcceso.Location = new System.Drawing.Point(125, 26);
            this.LblAcceso.Name = "LblAcceso";
            this.LblAcceso.Size = new System.Drawing.Size(43, 13);
            this.LblAcceso.TabIndex = 5;
            this.LblAcceso.Text = "Acceso";
            // 
            // bunifuImageButton5
            // 
            this.bunifuImageButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton5.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton5.Image")));
            this.bunifuImageButton5.ImageActive = null;
            this.bunifuImageButton5.Location = new System.Drawing.Point(37, 11);
            this.bunifuImageButton5.Name = "bunifuImageButton5";
            this.bunifuImageButton5.Size = new System.Drawing.Size(70, 68);
            this.bunifuImageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton5.TabIndex = 4;
            this.bunifuImageButton5.TabStop = false;
            this.bunifuImageButton5.Zoom = 10;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 15;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuElipse2
            // 
            this.bunifuElipse2.ElipseRadius = 8;
            this.bunifuElipse2.TargetControl = this.PanelContenedor;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.PanelSuperior;
            this.bunifuDragControl1.Vertical = true;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 700);
            this.Controls.Add(this.PanelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(960, 480);
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMenu";
            this.PanelPrincipal.ResumeLayout(false);
            this.PanelSuperior.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BtnRestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnCerrar)).EndInit();
            this.PanelSidebar.ResumeLayout(false);
            this.PanelSidebar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelPrincipal;
        private System.Windows.Forms.Panel PanelContenedor;
        private System.Windows.Forms.Panel PanelInferior;
        private System.Windows.Forms.Panel PanelSuperior;
        private System.Windows.Forms.Panel PanelSidebar;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuImageButton BtnCerrar;
        private Bunifu.Framework.UI.BunifuImageButton BtnRestaurar;
        private Bunifu.Framework.UI.BunifuImageButton BtnMaximizar;
        private Bunifu.Framework.UI.BunifuImageButton BtnMinimizar;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton5;
        private Bunifu.Framework.UI.BunifuThinButton2 BtnEditarPerfil;
        private System.Windows.Forms.Label LblUsuario;
        private System.Windows.Forms.Label LblAcceso;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.Framework.UI.BunifuFlatButton BtnDashboard;
        private Bunifu.Framework.UI.BunifuFlatButton BtnTamanos;
        private Bunifu.Framework.UI.BunifuFlatButton BtnPizzas;
        private Bunifu.Framework.UI.BunifuFlatButton BtnClientes;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse2;
        private Bunifu.Framework.UI.BunifuFlatButton BtnUsuarios;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
    }
}