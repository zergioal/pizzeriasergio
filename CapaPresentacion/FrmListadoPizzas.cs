﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CapaEntidades;
using CapaNegocios;

namespace CapaPresentacion
{
    public partial class FrmListadoPizzas : Form
    {
        readonly E_Pizzas ObjEntidad = new E_Pizzas();
        readonly N_Pizzas ObjNegocio = new N_Pizzas();
        public FrmListadoPizzas()
        {
            InitializeComponent();
        }

        private void MensajeConfirmacion(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MensajeError(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Pizzería Zizu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void AccionesTabla()
        {
            Data_pizzas.Columns[0].Visible = false;
            Data_pizzas.Columns[5].Visible = false;

            Data_pizzas.Columns[1].HeaderText = "Nombre";
            Data_pizzas.Columns[2].HeaderText = "Características";
            Data_pizzas.Columns[3].HeaderText = "Precio Venta";
            Data_pizzas.Columns[4].HeaderText = "Costo Producción";
            Data_pizzas.Columns[6].HeaderText = "Tamaño";

        }

        private void MostrarRegistros()
        {
            Data_pizzas.DataSource = N_Pizzas.MostrarRegistros();
            AccionesTabla();
        }

        public void BuscarRegistros()
        {
            Data_pizzas.DataSource = N_Pizzas.BuscarRegistros(TxtBuscar.Text);
        }

        private void ActualizarDatos(object sender, FormClosedEventArgs e)
        {
            MostrarRegistros();
        }

        public void ExportarDatos(DataGridView datalistado)
        {
            Microsoft.Office.Interop.Excel.Application exportarExcel = new Microsoft.Office.Interop.Excel.Application();
            exportarExcel.Application.Workbooks.Add(true);

            int indicecolumna = 0;

            foreach (DataGridViewColumn columna in datalistado.Columns)
            {
                indicecolumna++;

                exportarExcel.Cells[1, indicecolumna] = columna.Name;
            }

            int indicefila = 0;
            
            foreach (DataGridViewRow fila in datalistado.Rows)
            {
                indicefila++;
                indicecolumna = 0;

                foreach (DataGridViewColumn columna in datalistado.Columns)
                {
                    indicecolumna++;
                    exportarExcel.Cells[indicefila + 1, indicecolumna] = fila.Cells[columna.Name].Value;
                }
            }

            exportarExcel.Visible = true;
        }

        private void FrmListadoPizzas_Load(object sender, EventArgs e)
        {
            MostrarRegistros();
        }

        private void Pb_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            Program.Evento = 0;
            FrmPizzas nuevoregistro = new FrmPizzas();
            nuevoregistro.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            nuevoregistro.ShowDialog();
        }

        private void Btn_Editar_Click(object sender, EventArgs e)
        {
            FrmPizzas editarregistros = new FrmPizzas();
            editarregistros.FormClosed += new FormClosedEventHandler(ActualizarDatos);
            if(Data_pizzas.SelectedRows.Count>0)
            {
                Program.Evento = 1;
                editarregistros.TxtCodigo.Text = Data_pizzas.CurrentRow.Cells[0].Value.ToString();
                editarregistros.TxtNombre.Text = Data_pizzas.CurrentRow.Cells[1].Value.ToString();
                editarregistros.TxtCaracteristicas.Text = Data_pizzas.CurrentRow.Cells[2].Value.ToString();
                editarregistros.TxtPrecioVenta.Text = Data_pizzas.CurrentRow.Cells[3].Value.ToString();
                editarregistros.TxtCostoProduccion.Text = Data_pizzas.CurrentRow.Cells[4].Value.ToString();
                editarregistros.CbTamano.SelectedValue= Data_pizzas.CurrentRow.Cells[5].Value.ToString();
                editarregistros.ShowDialog();
            }
            else
            {
                MensajeError("Selecciona la fila a editar");
            }
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            if(Data_pizzas.SelectedRows.Count>0)
            {
                DialogResult opcion;
                opcion = MessageBox.Show("Realmente quiere eliminar el registro?", "Pizzeria Zizu", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if(opcion == DialogResult.OK)
                {
                    ObjEntidad.IdPizzas = Convert.ToInt32(Data_pizzas.CurrentRow.Cells[0].Value.ToString());

                    ObjNegocio.EliminarRegistros(ObjEntidad);

                    MensajeConfirmacion("Se eliminó correctamente");
                    MostrarRegistros();
                }

            }
            else
            {
                MensajeError("Seleccione un registro a eliminar");
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarRegistros();
        }

        private void Btn_Exportar_Click(object sender, EventArgs e)
        {
            ExportarDatos(Data_pizzas);
        }
    }
}
