﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using CapaEntidades;

namespace CapaDatos
{
    public class D_Usuarios
    {
        readonly SqlConnection conectar = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);

        /*public DataTable MostrarRegistros()
        {
            DataTable DtResultado = new DataTable();
            SqlCommand SqlCmd = new SqlCommand("spmostrar_clientes", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
            SqlDat.Fill(DtResultado);

            return DtResultado;
        }*/
        public List<E_Usuarios> ListarRegistros()
        {
            SqlDataReader LeerFilas;

            SqlCommand cmd = new SqlCommand("spmostrar_usuarios", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };
            conectar.Open();
            LeerFilas = cmd.ExecuteReader();
            List<E_Usuarios> Listar = new List<E_Usuarios>();

            while (LeerFilas.Read())
            {
                Listar.Add(new E_Usuarios
                {
                    IdUsuarios = LeerFilas.GetInt32(0),
                    Usuarios = LeerFilas.GetString(1),
                    Contraseña = LeerFilas.GetString(2),
                    Acceso = LeerFilas.GetString(3)

                });

            }

            conectar.Close();

            LeerFilas.Close();

            return Listar;
        }


        public void InsertarRegistros(E_Usuarios usuarios)
        {
            SqlCommand SqlCmd = new SqlCommand("spinsertar_usuarios", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@usuario", usuarios.Usuarios);
            SqlCmd.Parameters.AddWithValue("@contraseña", usuarios.Contraseña);
            SqlCmd.Parameters.AddWithValue("@acceso", usuarios.Acceso);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();

        }

        public void EditarRegistros(E_Usuarios usuarios)
        {
            SqlCommand SqlCmd = new SqlCommand("speditar_usuarios", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idusuarios", usuarios.IdUsuarios);
            SqlCmd.Parameters.AddWithValue("@usuario", usuarios.Usuarios);
            SqlCmd.Parameters.AddWithValue("@contraseña", usuarios.Contraseña);
            SqlCmd.Parameters.AddWithValue("@acceso", usuarios.Acceso);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }

        public void EliminarRegistros(E_Usuarios usuarios)
        {
            SqlCommand SqlCmd = new SqlCommand("speliminar_usuarios", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idusuarios", usuarios.IdUsuarios);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }
    }
}
