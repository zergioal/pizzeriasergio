﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using CapaEntidades;

namespace CapaDatos
{
    public class D_Clientes
    {
        readonly SqlConnection conectar = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);

        public DataTable MostrarRegistros()
        {
            DataTable DtResultado = new DataTable();
            SqlCommand SqlCmd = new SqlCommand("spmostrar_clientes", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
            SqlDat.Fill(DtResultado);

            return DtResultado;
        }
        public DataTable BuscarRegistros(string textobuscar)
        {
            DataTable DtResultado = new DataTable();
            SqlCommand SqlCmd = new SqlCommand("spbuscar_clientes2", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlCmd.Parameters.AddWithValue("@textobuscar", textobuscar);

            SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
            SqlDat.Fill(DtResultado);

            return DtResultado;
        }
        public void InsertarRegistros(E_Clientes clientes)
        {
            SqlCommand SqlCmd = new SqlCommand("spinsertar_clientes", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@nombre", clientes.Nombre);
            SqlCmd.Parameters.AddWithValue("@apellido", clientes.Apellido);
            SqlCmd.Parameters.AddWithValue("@cionit", clientes.CioNit);
            SqlCmd.Parameters.AddWithValue("@direccion", clientes.Direccion);
            SqlCmd.Parameters.AddWithValue("@telefono", clientes.Telefono);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();

        }

        public void EditarRegistros(E_Clientes clientes)
        {
            SqlCommand SqlCmd = new SqlCommand("speditar_clientes", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idclientes", clientes.IdClientes);
            SqlCmd.Parameters.AddWithValue("@nombre", clientes.Nombre);
            SqlCmd.Parameters.AddWithValue("@apellido", clientes.Apellido);
            SqlCmd.Parameters.AddWithValue("@cionit", clientes.CioNit);
            SqlCmd.Parameters.AddWithValue("@direccion", clientes.Direccion);
            SqlCmd.Parameters.AddWithValue("@telefono", clientes.Telefono);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }

        public void EliminarRegistros(E_Clientes clientes)
        {
            SqlCommand SqlCmd = new SqlCommand("speliminar_clientes", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idclientes", clientes.IdClientes);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }
    }
}
