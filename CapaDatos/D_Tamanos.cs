﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using CapaEntidades;
namespace CapaDatos
{
    public class D_Tamanos
    {
        readonly SqlConnection conectar = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);

        public DataTable MostrarRegistros()
        {
            DataTable DtResultado = new DataTable();
            SqlCommand SqlCmd = new SqlCommand("spmostrar_tamano", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
            SqlDat.Fill(DtResultado);

            return DtResultado;
        }

        public void InsertarRegistros(E_Tamanos tamanos)
        {
            SqlCommand SqlCmd = new SqlCommand("spinsertar_tamano", conectar)
            {  
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@descripcion",tamanos.Descripcion);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();

        }

        public void EditarRegistros(E_Tamanos tamanos)
        {
            SqlCommand SqlCmd = new SqlCommand("speditar_tamano", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idtamano", tamanos.IdTamano);
            SqlCmd.Parameters.AddWithValue("@descripcion", tamanos.Descripcion);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }

        public void EliminarRegistros(E_Tamanos tamanos)
        {
            SqlCommand SqlCmd = new SqlCommand("speliminar_tamano", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idtamano", tamanos.IdTamano);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }

    }
}
