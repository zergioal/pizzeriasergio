﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using CapaEntidades;

namespace CapaDatos
{
    public class D_InicioSesion
    {
        readonly SqlConnection conectar = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);

        public bool InicioSesion(string usuario, string contraseña)
        {
            SqlCommand sqlcmd = new SqlCommand("spiniciarsesion", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };
            conectar.Open();

            sqlcmd.Parameters.AddWithValue("@usuario", usuario);
            sqlcmd.Parameters.AddWithValue("@contraseña", contraseña);

            SqlDataReader LeerFilas = sqlcmd.ExecuteReader();

            if (LeerFilas.HasRows)
            {
                while (LeerFilas.Read())
                {
                    E_InicioSesion.IdUsuario = LeerFilas.GetInt32(0);
                    E_InicioSesion.Usuario = LeerFilas.GetString(1);
                    E_InicioSesion.Contraseña = LeerFilas.GetString(2);
                    E_InicioSesion.Acceso = LeerFilas.GetString(3);
                }
                conectar.Close();
                return true;
            }
            else
            {
                conectar.Close();
                return false;
            }

        }
    }
}
