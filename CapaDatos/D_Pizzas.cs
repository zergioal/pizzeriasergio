﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using CapaEntidades;

namespace CapaDatos
{
    public class D_Pizzas
    {
        readonly SqlConnection conectar = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);

        public DataTable MostrarRegistros()
        {
            DataTable DtResultado = new DataTable();
            SqlCommand SqlCmd = new SqlCommand("spmostrar_pizzacontamano2", conectar) //spmostrar_pizzas
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
            SqlDat.Fill(DtResultado);

            return DtResultado;
        }
        public DataTable BuscarRegistros(string textobuscar)
        {
            DataTable DtResultado = new DataTable();
            SqlCommand SqlCmd = new SqlCommand("spbuscar_pizzas", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlCmd.Parameters.AddWithValue("@textobuscar",textobuscar);

            SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
            SqlDat.Fill(DtResultado);

            return DtResultado;
        }
        public void InsertarRegistros(E_Pizzas pizzas)
        {
            SqlCommand SqlCmd = new SqlCommand("spinsertar_pizzas", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@nombre", pizzas.Nombre);
            SqlCmd.Parameters.AddWithValue("@caracteristicas", pizzas.Caracteristicas);
            SqlCmd.Parameters.AddWithValue("@precioventa", pizzas.PrecioVenta);
            SqlCmd.Parameters.AddWithValue("@costoproduccion", pizzas.CostoProduccion);
            SqlCmd.Parameters.AddWithValue("@idtamano", pizzas.IdTamano);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();

        }

        public void EditarRegistros(E_Pizzas pizzas)
        {
            SqlCommand SqlCmd = new SqlCommand("speditar_pizzas", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idpizzas", pizzas.IdPizzas);
            SqlCmd.Parameters.AddWithValue("@nombre", pizzas.Nombre);
            SqlCmd.Parameters.AddWithValue("@caracteristicas", pizzas.Caracteristicas);
            SqlCmd.Parameters.AddWithValue("@precioventa", pizzas.PrecioVenta);
            SqlCmd.Parameters.AddWithValue("@costoproduccion", pizzas.CostoProduccion);
            SqlCmd.Parameters.AddWithValue("@idtamano", pizzas.IdTamano);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }

        public void EliminarRegistros(E_Pizzas pizzas)
        {
            SqlCommand SqlCmd = new SqlCommand("speliminar_pizzas", conectar)
            {
                CommandType = CommandType.StoredProcedure
            };

            conectar.Open();

            SqlCmd.Parameters.AddWithValue("@idpizzas", pizzas.IdPizzas);

            SqlCmd.ExecuteNonQuery();

            conectar.Close();
        }
    }
}
